var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin'),
    cache = require('gulp-cache');
var minifycss = require('gulp-minify-css');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var order = require('gulp-order');

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: "./"
        }
    });
});

gulp.task('bs-reload', function () {
    browserSync.reload();
});

gulp.task('images', function(){
    gulp.src('resources/img/**/*')
        .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
        .pipe(gulp.dest('public/img/'));
});

gulp.task('fonts', function(){
    gulp.src('resources/fonts/**/*')
        .pipe(gulp.dest('public/fonts/'));
});

gulp.task('styles', function(){
    gulp.src(['resources/css/**/*.css','resources/scss/main.scss'])
        .pipe(plumber({
            errorHandler: function (error) {
                console.log(error.message);
                this.emit('end');
            }}))
        .pipe(sass())
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(concat('main.css'))
        .pipe(gulp.dest('public/css/'));
});

gulp.task('scripts', function(){
    return gulp.src('resources/js/**/*.js')
        .pipe(order(['jquery.js', 'bootstrap.js', '*.js']))
        .pipe(concat('main.js'))
        .pipe(gulp.dest('./public/js/'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('./public/js/'));
     //   .pipe(browserSync.reload({stream:true}))
});

gulp.task('default', function(){
    gulp.watch("resources/scss/**/*.scss", ['styles']);
    gulp.watch("resources/js/**/*.js", ['scripts']);
    gulp.watch("*.html", ['bs-reload']);
});