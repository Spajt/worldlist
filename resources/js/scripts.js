$(document).ready( function () {
    JSON.canParse = function(string) {
      try {
          JSON.parse(string);
          return true;
      } catch (e) {
          return false;
      }
    };
    function queryParams() {
        var page = 0;
        var limit = 20;

        return {
            "page" : page,
            "limit" : limit
        }
    }

    function enableLoader() {
        var loader = $("#loader").clone();
            loader.prop('id', 'activeLoader');
            loader.css('display','block');
        $('.table').parent().prepend(loader);
    }

    function disableLoader() {
        $('#activeLoader').remove();
    }

    function textItem(item, params, type) {
        var textObj = $("<div></div>");
        if (item.attr('data-options')) {
            textObj.attr('data-options', item.attr('data-options'));
        }
        textObj.attr('data-name', item.prop('name'));
        if (params && params.value) {
            textObj.text(params.value);
        } else {
            textObj.text(item.val());
        }
        var parent = item.parent();
        if (type === "appendTo") {
            return textObj;
        }
        item.replaceWith(textObj);
        textObj.siblings('span.select2').remove();
    }

    function updateItem(item) {
        var params = {
            "value" : item.val(),
            "id"    : item.closest('.row').data('id'),
            "fieldName"  : item.prop('name')
        };
        var url = item.closest('.table').data('entity')+"/update";
        $.ajax(url, {
            method : "POST",
            data   : params
        }).done( function (msg) {
            if (JSON.canParse(msg)) {
                var msgObj = JSON.parse(msg);
                if (msgObj.success) {

                        textItem(item, params);
                }
            }
        }).fail( function (msg) {
            var textObj = $("<div></div>");
            if (item.attr('data-options')) {
                textObj.attr('data-options', item.attr('data-options'));
            }
            textItem(item, params);
        });

    }

    function createSelect(item) {
            var options = item.data('options');

            if (typeof options !== "array") {
                options = options.split(',');
            }

            var select = $("<select></select>");
            select.prop('name', item.data('name'));
            for (var a = 0; a < options.length; a++) {
                var value,label;
                if (typeof options[a] === "object") {
                    value = Object.keys(options[a])[0];
                    label = options[a][value];
                } else {
                    value = options[a];
                    label = options[a];
                }
                var option = $("<option></option>");
                option.prop('value', value);
                option.html(label);
                select.attr('data-options', item.data('options'));
                select.append(option);
            }
            var val = item.html();
           // select.select2();
            select.attr('data-original-value', item.html());
            console.log(select);
            item.replaceWith(select);
            select.select2();

    }

    function createInput(item) {
        var input = $("<input />");
        input.prop('name', item.data('name'));
        input.val(item.text());
        input.attr('data-original-value', item.html());
        item.replaceWith(input);
        }

    $(document).on('dblclick', '.table [data-name], .table [data-name*=""]' , function () {
       switch ($(this).data('type')) {
           case "select" :
               createSelect($(this));
               break;
           case "input" :
               createInput($(this));
           break;
           default :
               createInput($(this));
       }
    });

    $('nav a').click( function (e) {
       e.preventDefault();
       var url = $(this).attr('href');

        $('.active').removeClass('active');
        $('[data-page]').eq(0).addClass('active');
        $('[data-limit]').eq(0).addClass('active');

       $.ajax(url, {
           method : "POST",
           data : queryParams(),
           beforeSend : function () {
                enableLoader();
           },
           success: function(msg) {
               $('.table').html(msg);
               $('.table').eq(0).data('entity', url);
               disableLoader();
               window.history.pushState({},"",url);
           },
           error: function(msg) {
               disableLoader();
           }
       });

    });

    $('.search-name').change( function () {
        var url = $('.table').data('entity');

        $.ajax(url, {
            method : "POST",
            data : {
                name : $(this).val()
            }
        }).done( function (msg) {
            if (typeof msg === "object") {
                $('.table').html(msg.body);
            } else if (typeof msg === "string") {
                $('.table').html(msg);
            }
        });
    });

    $(document).on('click' , '[data-limit],[data-limit*=""],[data-page],[data-page*=""]', function (e) {
        e.preventDefault();

        $(this).parent().parent().find('.active').removeClass('active');
        $(this).addClass('active');
        var url = $('.table').data('entity');
        var limit = $('[data-limit].active').eq(0).data('limit');
        var page = $('[data-page].active').eq(0).data('page');
        $.ajax(url, {
            method : "POST",
            data : {
                limit : limit,
                offset  : page
            }
        }).done( function (msg) {
            if (typeof msg === "object") {
                $('.table').html(msg.body);
            } else if (typeof msg === "string") {
                $('.table').html(msg);
            }
        });
    });

    $(document).on('blur', '.table .row input', function () {
            updateItem($(this));
    });
    $(document).on('change', '.table .row .cell select', function () {
        updateItem($(this));
    });
    $(document).on('click', '.delete', function () {
            var id = $(this).closest('.row').data('id');
            var url = $(this).closest('.table').data('entity');
            var item = $(this);
            $.ajax(url+'/delete'+"/"+id, {
                method: "DELETE"
            }).done(function (msg) {
                if (typeof msg === "string" && !JSON.canParse(msg))  return false;
                if (msg.success == true) {
                    item.closest('.row').addClass('deleting');
                    item.closest('.row').fadeOut(750, function () {
                        $(this).remove();
                    });
                }
            });
    });
    $(document).on('click', '.save', function () {
        var url = $(this).closest('.table').data('entity');
        var item = $(this);
        var params = {};
        item.closest('.row').find('input,select').each( function () {
           params[$(this).prop('name')] = $(this).val();
        });
        $.ajax(url+'/create', {
            method: "POST",
            data : params
        }).done(function (msg) {
            if (typeof msg === "string" && !JSON.canParse(msg))  return false;
            if (msg.success == true) {
                var row = item.closest('.row');
                var cloneRow = row.clone();
                row.find('input,select').each(function () {
                    $(this).replaceWith(textItem($(this), false, "appendTo"));
                });
                row.find(".glyphicon-floppy-saved.save")
                    .removeClass('save')
                    .removeClass('glyphicon-floppy-saved')
                    .addClass('glyphicon-floppy-remove')
                    .addClass('delete');
                row.closest('.table').append(cloneRow);
            }
        }).fail(function () {
            var row = item.closest('.row');
            var cloneRow = row.clone();
            row.find('input,select').each(function () {
               $(this).replaceWith(textItem($(this), false, "appendTo"));
            });
            row.find(".glyphicon-floppy-saved.save")
                .removeClass('save')
                .removeClass('glyphicon-floppy-saved')
                .addClass('glyphicon-floppy-remove')
                .addClass('delete');
            row.closest('.table').append(cloneRow);
        });
    });
});