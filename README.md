#Jak instalować:
1. klonujemy repo
2. cp config/autoload/local.php.dist config/autoload/local.php
3. uzupełniamy dane bazy
4. tworzymy bazę
5. sudo bash install.sh
6. podajemy docelową domenę
7. gotowe

W katalogu znajduje się klasyczna testowa baza MySQL - world.sql. Baza ta zawiera informacje o miastach, państwach i językach.
Celem zadania będzie wyświetlenie listy miast i państw, a także możliwość zarządzania nimi. Listy powinny obsługiwać paginację,
sortowanie i wyszukiwanie po nazwie. Edytować można miasta i państwa. Wszystkie dane powinny być wyciągane i zapisywane z bazy.

Backend powinien być napisany w PHP z użyciem frameworka Zend Framwork 2.4.
Front powinien używać Bootstrap 3.

###Mile widziane:

Używanie composera.

OOP, 

SOLID-ny kod.

Normalizacja w bazie banych.

Odpowiednia walidacja pól formularzy.

###Dodatkowe punkty za:

Stosowanie SCSS i gulpa.

Stosowanie AJAXa do list i formularzy.

RWD.
