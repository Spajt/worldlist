<?php
/**
 * Created by PhpStorm.
 * User: spite
 * Date: 2016-11-22
 * Time: 18:38
 */

namespace System\Controller;


use Application\Form\City;
use DoctrineORMModule\Form\Annotation\AnnotationBuilder;
use DoctrineORMModule\Service\DoctrineObjectHydratorFactory;
use System\Exceptions\BadMethodException;
use Zend\Form\FormInterface;
use Zend\I18n\Translator\Translator;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\Validator\AbstractValidator;
use Zend\Validator\GreaterThan;
use Zend\View\Helper\Json;
use Zend\View\Helper\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\View\View;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;


class SystemController extends AbstractActionController
{
    const HTTP_POST     = "POST";
    const HTTP_GET      = "GET";
    const HTTP_PUT      = "PUT";
    const HTTP_PATCH    = "PATCH";
    const HTTP_DELETE   = "DELETE";

    /**
     * Nazwa obsługiwanego entity
     *
     * @var string
     */
    protected $entityName;


    /**
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        return $this->getListAction();
    }

    /**
     * @return \Zend\View\Model\ViewModel
     */
    public function getListAction()
    {
        $page  = $this->getRequest()->getPost('offset', 0);
        $limit = $this->getRequest()->getPost('limit', 20);
        $name = $this->getRequest()->getPost('name', false);

        if (!$name) {
            $entity = $this->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager')
                ->getRepository($this->getEntityName())
                ->findAll($page * $limit, $limit);
        } else {
            $entity = $this->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager')
                ->getRepository($this->getEntityName())
                ->findByName($name, $page * $limit, $limit);
        }
        $count = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository($this->getEntityName())
            ->count();
        // Lista polaczonych entity dla naszego Entity
        $associated = [];

        if (count($entity) && method_exists($entity[0], 'getAssociated')) {
            foreach ($entity[0]->getAssociated() AS $key => $association) {
                // If blokujacy dublowanie zapytań
                if (!isset($associated[$key])) {
                    $associated[$key] = $this->getServiceLocator()
                        ->get('Doctrine\ORM\EntityManager')
                        ->getRepository($association)
                        ->findAll(0, null);
                }
            }
        }

        $class =  $this->getEntityName();

        // Render
        $view = new \Zend\View\Model\ViewModel(array(
            'entities' => $entity,
            'associated' => $associated,
            'count' => $count,


            'associated' => $associated,
            'createRoute' => $class::getCreateRoute(),
        ));
        $view->setTemplate('system/get-list.phtml');

        $this->layout()->setVariables(['limit' => $limit, 'offset' => $page, 'count' => $count, 'em' => $this->getEntityNamePlural()]);
        // Jezeli żądanie ajaxowe, to omijamy layout
        if ($this->getRequest()->isXmlHttpRequest()) {
            $view->setTerminal(true);
        }

        return $view;
    }

    /**
     * @return \Zend\View\Model\ViewModel
     */
    public function createAction()
    {
        $translator = $this->serviceLocator->get('MvcTranslator');
        $translator->addTranslationFile(
            'phpArray',
            'resources/languages/pl/Zend_Validate.php',
            'default',
            'pl_PL'
        );
        AbstractValidator::setDefaultTranslator($translator);

        $entityManager = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $repository = $entityManager
            ->getRepository($this->getEntityName());

        $count = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository($this->getEntityName())
            ->count();

        $class = $this->getEntityName();

        $entity = new $class();

        $builder    = new \Zend\Form\Annotation\AnnotationBuilder();
        $form       = $builder->createForm($entity);
        $form->setHydrator(new DoctrineHydrator(
            $entityManager,
            $this->getEntityName()
        ));


        // Wypelnia selecty opcjami
        if (count($entity->getAssociated())) {
            foreach ($entity->getAssociated() AS $key => $association) {
                $element = $form->get($key);

                if ($element) {
                    $form->add(
                        array(
                            'name' => $key,
                            'type' => 'DoctrineORMModule\Form\Element\DoctrineEntity',
                            'options' => array(
                                'label'          => $key,
                                'object_manager' => $entityManager,
                                'target_class'   => $association,
                                'property'       => $association::getIdColumn(),
                                'find_method' => array(
                                    'name'   => 'findAll',
                                ),
                            ),
                            'attributes' => [
                                'class' => 'form-control'
                            ]
                        ));
                }
            }
        }

        $form->bind($entity);


        // Dodaj submita do formularza
        $form->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'class' => 'btn btn-info',
                'value' => 'Zapisz',
                'id' => 'createSubmit',
            ),
        ));

        // Po Submit formularza
        $request = $this->getRequest();
        if ($request->isPost()) {
            //$ = new \Application\Entity\City();
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $data = $form->getData();

                // Lista pol ktore moga zostac ustawione
                $entityForm = $entity->toArray();

                foreach ($data AS $key => $val) {
                    if (array_key_exists($key, $entityForm)) {
                        $method = 'set' . ucfirst($key);
                        $entity->$method($val);
                    }
                }

                $entityManager->persist($entity);
                $entityManager->flush();

                return $this->redirect()->toRoute($entity->getViewRoute(), array('id'=> $entity->getId()));
            }
        }

        // Render
        $view = new \Zend\View\Model\ViewModel(array(
            'form' => $form,
        ));
        $view->setTemplate('system/create.phtml');


        // Jezeli żądanie ajaxowe, to omijamy layout
        if ($this->getRequest()->isXmlHttpRequest()) {
            $view->setTerminal(true);
        }

        return $view;
    }

    /**
     * @return \Zend\View\Model\ViewModel
     */
    public function updateAction()
    {
        $request = $this->getRequest();

        $translator = $this->serviceLocator->get('MvcTranslator');
        $translator->addTranslationFile(
            'phpArray',
            'resources/languages/pl/Zend_Validate.php',
            'default',
            'pl_PL'
        );
        AbstractValidator::setDefaultTranslator($translator);

        $entityManager = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $repository = $entityManager
            ->getRepository($this->getEntityName());

        $entity = $repository->find($request->getPost('id'));
        $entity->setServiceLocator($this->getServiceLocator());


        $builder    = new \Zend\Form\Annotation\AnnotationBuilder();
        $form       = $builder->createForm($entity);
        $form->setHydrator(new DoctrineHydrator(
            $entityManager,
            $this->getEntityName()
        ));

        // Wypelnia selecty opcjami
        if (count($entity->getAssociated())) {
            foreach ($entity->getAssociated() AS $key => $association) {
                $element = $form->get($key);

                if ($element) {
                    $form->add(
                        array(
                            'name' => $key,
                            'type' => 'DoctrineORMModule\Form\Element\DoctrineEntity',
                            'options' => array(
                                'label'          => $key,
                                'object_manager' => $entityManager,
                                'target_class'   => $association,
                                'property'       => $association::getIdColumn(),
                                'find_method' => array(
                                    'name'   => 'findAll',
                                ),
                            ),
                            'attributes' => [
                                'class' => 'form-control'
                            ]
                        ));
                }
            }
        }

        $form->bind($entity);

        $response = null;

        // Po Submit
        if ($request->isPost()) {
            $postData = $request->getPost();

            $postData = $postData->Set($request->getPost('fieldName'), $request->getPost('value'));


            $form->setData($postData);

            $form->isValid();

            $messages = $form->getMessages($request->getPost('fieldName'));

            if (count($messages)) {
                $response['error'] = true;
                foreach ($messages AS $msg) {
                    $response[] = $translator->translate($msg);
                }
            } else {
                $method = 'set' . ucfirst($request->getPost('fieldName'));

                $entity->$method($request->getPost('value'));

                $entityManager->persist($entity);
                $entityManager->flush();


                $response = [
                    'success' => 1,
                    $request->getPost('fieldName') => $request->getPost('value')
                ];
            }
        }

        // Render
        $view = new \Zend\View\Model\ViewModel(array(
            'data' => $response,
        ));
        $view->setTemplate('system/json.phtml');


        // omijamy layout
        $view->setTerminal(true);

        return $view;
    }

    /**
     * Akcja przyjmująca id Entity City, wyświetla dane o obiekcie
     * @return \Zend\View\Model\ViewModel
     */
    public function showAction() {
        $id = $this->params('id');

        $entity = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->find($this->getEntityName(), $id);

        $view = new \Zend\View\Model\ViewModel(array(
            'entity' => $entity
        ));
        $view->setTemplate('system/show.phtml');

        // Jezeli żądanie ajaxowe, to omijamy layout
        if ($this->getRequest()->isXmlHttpRequest()) {
            $view->setTerminal(true);
        }



        return $view;
    }

    public function deleteAction() {
        $id = $this->params('id');

        $em = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $entity = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->find($this->getEntityName(), $id);

        $em->remove($entity);
        $em->flush();
        $response = new JsonModel([
            'success' => true
        ]);

        return $response;
    }

    /**
     * Metoda kontroluje dostęp różnych rodzajów żądań
     *
     * @param MvcEvent $e
     * @return mixed
     * @throws BadMethodException
     */
    public function onDispatch(MvcEvent $e)
    {
        $routeMethod = $e->getRouteMatch()->getParam('method', self::HTTP_GET);
        $requestMethod = $e->getRequest()->getMethod();
        if ((is_array($routeMethod) && !in_array($requestMethod, $routeMethod)) || (!is_array($routeMethod) && $routeMethod !== $requestMethod)) {
            throw new BadMethodException('This method is not allowed for that route');
        }
        return parent::onDispatch($e);
    }

    /**
     * Zwraca nazwe Entity, który jest obsługiwany kontrolerem
     * @return string
     */
    public function getEntityName() {
        return $this->entityName;
    }

    /**
     * Zwraca nazwe Entity, który jest obsługiwany kontrolerem
     * @return string
     */
    public function getEntityNamePlural() {
        return $this->entityNamePlural;
    }
}