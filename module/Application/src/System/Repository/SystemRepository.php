<?php
/**
 * Created by PhpStorm.
 * User: spite
 * Date: 2016-11-21
 * Time: 22:50
 */

namespace System\Repository;




use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityRepository;

class SystemRepository extends EntityRepository
{
    const COLUMN_NAMING = "Name";

    /**
     * @param string $name
     * @param int $offset
     * @param int $limit
     * @return array of Entity
     */
    public function findByName($name = "", $offset = 0, $limit = 20) {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->contains(self::COLUMN_NAMING, $name));
        $e = $this->getEntityManager()->getRepository($this->getEntityName())->matching($criteria)->slice($offset*$limit, $limit);
        return $e;
    }


    public function findAll($offset = 0, $limit = 20) {
        return $this->findBy([], [], $limit, $offset);
    }

    public function count() {
        return count(parent::findAll());
    }
}