<?php
/**
 * Created by PhpStorm.
 * User: spite
 * Date: 2016-11-22
 * Time: 18:59
 */

namespace System\Exceptions;


use Zend\Http\Exception\ExceptionInterface;

class BadMethodException extends \Exception implements ExceptionInterface
{
    public function __construct($e, $status = 500, \Exception $prev = null)
    {
        throw new \Exception($e, $status, $prev);
    }
}