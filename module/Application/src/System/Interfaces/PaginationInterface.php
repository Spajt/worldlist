<?php
namespace System\Interfaces;

interface PaginationInterface {
         function addPagination($page = 1, $perPage = 20);
}