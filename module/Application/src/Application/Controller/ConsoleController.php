<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Entity\City;
use Doctrine\ORM\EntityManager;
use Application\Entity\User;
use Zend\Console\Prompt\Line;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Console\Request as ConsoleRequest;

class ConsoleController extends AbstractActionController
{

    public function installAction() {

    $request = $this->getRequest();
    if (!$request instanceof ConsoleRequest) { $this->redirect('404'); }


    echo system('php public/index.php dbal:import data/seeds/world.sql');

    $this->installVhost();
        echo system('chmod 777 -R '.ROOT_PATH.'/data/DoctrineORMModule');
    }

    private function installVhost() {
        if (is_dir('/etc/apache2/sites-available')) {
            if (posix_getuid() !== 0) {
                echo "You are not sudo priviliges";
                return false;
            }
            $domain =  Line::prompt('Enter domain: ', false);
            if (strpos($domain, '.') < 0) { return false;}
            $file = '/etc/apache2/sites-available/'.$domain.'.conf';
            if (file_exists($file)) { echo "Vhost exists".PHP_EOL; return false;}
            file_put_contents($file, $this->getVhostConf($domain));
            echo system('a2ensite '.$domain.".conf");
            echo system('service apache2 reload');

        } else {
            echo "your webserver is not Apache2";
            return false;
        }
        return true;
    }

    private function getVhostConf($domain) {
        $vhostConf = '<VirtualHost *:80> 
                        ServerAdmin webmaster@localhost
                            ServerName '.$domain.'
                            ServerAlias www.'.$domain.'
                            DocumentRoot '.ROOT_PATH.'/public/           
                        </VirtualHost>
                        ';
        return $vhostConf;
    }
}
