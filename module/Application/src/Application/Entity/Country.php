<?php
namespace Application\Entity;
use Application\Entity\EntityInterface\ArrayEntity;
use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation;


/**
 * @ORM\Entity(repositoryClass="Application\Repository\CountryRepository")
 * @ORM\Table(name="country", indexes={@ORM\Index(name="search_idx", columns={"Name", "Continent", "Region"})})
 */
class Country implements ArrayEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="string", name="Code", columnDefinition="CHAR(3)")
     * @Annotation\Filter({"name":"StringTrim"})
     * @Annotation\Validator({"name":"StringLength", "options":{"min":1, "max":3}})
     * @Annotation\Attributes({"type":"text", "class":"form-control"})
     * @Annotation\Options({"label":"Code:"})
     */
    protected $Code;

    /**
     * @ORM\Column(type="string", columnDefinition="CHAR(52) NOT NULL")
     * @Annotation\Validator({"name":"StringLength", "options":{"min":1, "max":52}})
     * @Annotation\Attributes({"type":"text", "class":"form-control"})
     * @Annotation\Options({"label":"Name:"})
     */
    protected $Name;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('Asia','Europe','North America','Africa','Oceania','Antarctica','South America') NOT NULL DEFAULT 'Asia'")
     * @Annotation\Type("Zend\Form\Element\Select")
     * @Annotation\Options({"label":"Continent"})
     * @Annotation\Attributes({"options":{"Asia":"Asia","Europe":"Europe","North America":"North America","Africa":"Africa","Oceania":"Oceania","Antarctica":"Antarctica","South America":"South America"}})

     */
    protected $Continent = 'Asia';

    /**
     * @ORM\Column(type="string", columnDefinition="CHAR(26) NOT NULL")
     * @Annotation\Validator({"name":"StringLength", "options":{"min":1, "max":26}})
     * @Annotation\Attributes({"type":"text", "class":"form-control"})
     * @Annotation\Options({"label":"Region:"})
     */
    protected $Region;

    /**
     * @ORM\Column(type="float", columnDefinition="FLOAT(10,2) NOT NULL")
     */
    protected $SurfaceArea = 0.00;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    protected $IndepYear;

    /**
     * @ORM\Column(type="integer", nullable=false, options={"default"=0})
     */
    protected $Population = 0;

    /**
     * @ORM\Column(type="float", columnDefinition="FLOAT(3,1)")
     */
    protected $LifeExpectancy = 0.00;

    /**
     * @ORM\Column(type="float", columnDefinition="FLOAT(10,2)")
     */
    protected $GNP;

    /**
     * @ORM\Column(type="float", columnDefinition="FLOAT(10,2)")
     */
    protected $GNPOld;

    /**
     * @ORM\Column(type="string", columnDefinition="CHAR(45) NOT NULL")
     */
    protected $LocalName;

    /**
     * @ORM\Column(type="string", columnDefinition="CHAR(45) NOT NULL")
     */
    protected $GovernmentForm;

    /**
     * @ORM\Column(type="string", columnDefinition="CHAR(60)")
     */
    protected $HeadOfState;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $Capital;

    /**
     * @ORM\Column(type="string", columnDefinition="CHAR(2) NOT NULL")
     */
    protected $Code2;

    public function setCode($code) {
        $this->Code = $code;
    }

    public function setName($name) {
        $this->Name = $name;
    }

    public function setContinent($continent) {
        $this->Continent = $continent;
    }

    public function setRegion($region) {
        $this->Region = $region;
    }

    public function setSurfaceArea($surfaceArea) {
        $this->SurfaceArea = $surfaceArea;
    }

    public function setIndepYear($indepYear) {
        $this->IndepYear = $indepYear;
    }

    public function setPopulation($population) {
        $this->Population = $population;
    }

    public function setLifeExpectancy($lifeExpectancy) {
        $this->LifeExpectancy = $lifeExpectancy;
    }

    public function setGNP($gnp) {
        $this->GNP = $gnp;
    }

    public function setGNPOld($gnpOld) {
        $this->GNPOld = $gnpOld;
    }

    public function setLocalName($localName) {
        $this->LocalName = $localName;
    }

    public function setGovernmentForm($govForm) {
        $this->GovernmentForm = $govForm;
    }

    public function setHeadOfState($hos) {
        $this->HeadOfState = $hos;
    }

    public function setCapital($capital) {
        $this->Capital = $capital;
    }

    public function setCode2($code2) {
        $this->Code2 = $code2;
    }

    /**
     * @return string
     */
    public static function getIdColumn() {
        return 'Code';
    }

    /**
     * @return mixed
     */
    public function getCode() {
        return $this->Code;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->Code;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return '/country/' . $this->getId();
    }

    public function getViewRoute() {
        return 'countryShow';
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'Code' => $this->Code,
            'Name' => $this->Name,
            'Continent' => $this->Continent,
            'Region' => $this->Region,
            'SurfaceArea' => $this->SurfaceArea,
            'IndepYear' => $this->IndepYear,
            'Population' => $this->Population,
            'LifeExpectancy' => $this->LifeExpectancy,
            'GNP' => $this->GNP,
            'GNPOld' => $this->GNPOld,
            'LocalName' => $this->LocalName,
            'GovernmentForm' => $this->GovernmentForm,
            'HeadOfState' => $this->HeadOfState,
            'Capital' => $this->Capital,
            'Code2' => $this->Code2
        ];
    }

    public function getAssociated() {
        return [];
    }

    public static function getCreateRoute()
    {
        return 'countryCreate';
    }
}