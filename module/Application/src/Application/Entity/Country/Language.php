<?php
namespace Application\Entity\Country;
use Application\Entity\Country;
use Application\Entity\EntityInterface\ArrayEntity;
use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation;



/**
 * @ORM\Entity(repositoryClass="Application\Repository\LanguageRepository")
 * @ORM\Table(name="countrylanguage")
 */
class Language implements ArrayEntity
{
    /**
     * @ORM\Column(type="string", name="CountryCode", columnDefinition="CHAR(3)")
     * @ORM\ManyToOne(targetEntity="Application\Entity\Country")
     * @ORM\JoinColumn(name="CountryCode", referencedColumnName="Code")
     */
    protected $country;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="string", columnDefinition="CHAR(30) NOT NULL DEFAULT ''")
     * @Annotation\Filter({"name":"StringTrim"})
     * @Annotation\Validator({"name":"StringLength", "options":{"min":1, "max":30}})
     * @Annotation\Attributes({"type":"text"})
     * @Annotation\Options({"label":"Language:"})
     */
    protected $Language;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('T', 'F') NOT NULL DEFAULT 'F'")
     * @Annotation\Type("Zend\Form\Element\Select")
     * @Annotation\Options({"label":"Description"})
     * @Annotation\Attributes({"options":{"T":"True","F":"False"}})
     */
    protected $isOfficial = 'F';

    /**
     * @ORM\Column(type="float", columnDefinition="FLOAT(4,1) NOT NULL DEFAULT 0.0")
     * @Annotation\Validator({"name":"Float"})
     * @Annotation\Validator({"name":"NotEmpty"})
     * @Annotation\Attributes({"type":"text"})
     * @Annotation\Options({"label":"Percentage:"})
     */
    protected $Percentage;

    public function setCountry($country)
    {
        $this->country = $country;
    }

    public function setLanguage($language) {
        $this->Language = $language;
    }

    public function setIsofficial($isOfficial)
    {
        $this->isOfficial = $isOfficial;
    }

    public function setPercentage($percentage)
    {
        $this->Percentage = $percentage;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->Language;
    }

    /**
     * @return float
     */
    public function getPercentage()
    {
        return $this->Percentage;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return null;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'Language' => $this->Language,
            'country' => $this->country,
            'isOfficial' => $this->isOfficial,
            'Percentage' => $this->Percentage,
        ];
    }

    /**
     * @return array
     */
    public function getAssociated()
    {
        return [
            'country' => Country::class
        ];
    }

    public function getViewRoute() {
        return 'languageShow';
    }

    public static function getCreateRoute()
    {
        return 'languageCreate';
    }
}