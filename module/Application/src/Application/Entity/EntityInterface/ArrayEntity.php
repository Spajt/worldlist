<?php
namespace Application\Entity\EntityInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Interface ArrayEntity
 * @package Application\EntityInterface
 *
 * Interfejs ArrayEntity wymusza na Entity możliwość prezentacji
 * za pomocą tablicy
 */
interface ArrayEntity
{
    /**
     * @return array
     */
    public function toArray();
}