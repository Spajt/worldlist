<?php
namespace Application\Entity;
use Application\Entity\EntityInterface\ArrayEntity;
use Application\Entity\EntityInterface\CrudEntity;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Mapping as ORM;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Zend\Validator\NotEmpty;
use Zend\Validator\StringLength;
use Zend\Form\Annotation;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;



/**
 * @ORM\Entity(repositoryClass="Application\Repository\CityRepository")
 * @ORM\Table(name="city")
 */
class City implements ArrayEntity
{
    /**
     * @Annotation\Exclude()
     */
    protected $serviceLocator = null;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Annotation\Exclude()
     */
    protected $Id;

    /**
     * @ORM\Column(type="string", columnDefinition="CHAR(35) NOT NULL")
     * @Annotation\Filter({"name":"StringTrim"})
     * @Annotation\Validator({"name":"StringLength", "options":{"min":1, "max":32}})
     * @Annotation\Attributes({"type":"text", "class":"form-control"})
     * @Annotation\Options({"label":"Name:"})
     */
    protected $Name;

    /**
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumn(name="CountryCode", referencedColumnName="Code")}
     * @Annotation\Type("Zend\Form\Element\Select")
     */
    private $country;

    /**
     * @ORM\Column(type="string", columnDefinition="CHAR(20) NOT NULL")
     * @Annotation\Filter({"name":"StringTrim"})
     * @Annotation\Validator({"name":"StringLength", "options":{"min":1, "max":20}})
     * @Annotation\Attributes({"type":"text", "class":"form-control"})
     * @Annotation\Options({"label":"District:"})

     */
    protected $District;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @Annotation\Validator({"name":"Int"})
     * @Annotation\Validator({"name":"NotEmpty"})
     * @Annotation\Attributes({"type":"text", "class":"form-control"})
     * @Annotation\Options({"label":"Population:"})
     */
    protected $Population;


    /**
     * @return mixed
     */
    public function getName() {
        return $this->Name;
    }

    /**
     * @param string $Name
     */
    public function setName($Name) {
        $this->Name = $Name;
    }

    /**
     * @param string $District
     */
    public function setDistrict($District) {
        $this->District = $District;
    }

    /**
     * @param string $country
     */
    public function setCountry($country) {
        if (!is_object($country)) {
            $entity = $this->serviceLocator
                ->get('Doctrine\ORM\EntityManager')
                ->find(Country::class, 'AND');

            $this->country = $entity;
            return;
        }

        $this->country = $country;
    }

    /**
     * @param integer $Population
     */
    public function setPopulation($Population) {
        $this->Population = $Population;
    }

    /**
     * @return integer
     */
    public function getId() {
        return $this->Id;
    }

    /**
     * @return string
     */
    public function getUrl() {
        return '/city/' . $this->getId();
    }

    public function setServiceLocator($sl) {
        $this->serviceLocator = $sl;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'Id' => $this->Id,
            'Name' => $this->Name,
            'country' => $this->country,
            'District' => $this->District,
            'Population' => $this->Population
        ];
    }

    /**
     * @return array
     */
    public function getAssociated()
    {
        return [
            'country' => Country::class
        ];
    }

    public function getViewRoute() {
        return 'cityShow';
    }

    public static function getCreateRoute()
    {
        return 'cityCreate';
    }
}