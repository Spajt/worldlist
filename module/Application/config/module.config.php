<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Country',
                        'action'     => 'index',
                        'method'     => [\System\Controller\SystemController::HTTP_GET, \System\Controller\SystemController::HTTP_POST],
                        'limit'      => 20,
                        'offset'     => 0
                    ),
                ),

            ),
            'cityRecords' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/cities',
                    'defaults' => array(
                        'controller' => 'Application\Controller\City',
                        'action'     => 'getList',
                        'method'     => [\System\Controller\SystemController::HTTP_GET, \System\Controller\SystemController::HTTP_POST],
                        'limit'      => 20,
                        'offset'     => 0
                    ),
                ),
            ),
            'cityShow' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/city/:id[/]',
                    'constraints' => [
                        'id' => '[0-9]+'
                    ],
                    'defaults' => array(
                        'controller' => 'Application\Controller\City',
                        'action'     => 'show',
                        'method'     => [\System\Controller\SystemController::HTTP_GET, \System\Controller\SystemController::HTTP_POST],
                    ),
                ),
            ),
            'cityUpdate' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/city/update',
                    'defaults' => array(
                        'controller' => 'Application\Controller\City',
                        'action'     => 'update',
                        'method'     => [\System\Controller\SystemController::HTTP_POST],
                    ),
                ),
            ),
            'cityUpdate2' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/cities/update',
                    'defaults' => array(
                        'controller' => 'Application\Controller\City',
                        'action'     => 'update',
                        'method'     => [\System\Controller\SystemController::HTTP_POST],
                    ),
                ),
            ),
            'cityCreate' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/city/create',
                    'defaults' => array(
                        'controller' => 'Application\Controller\City',
                        'action'     => 'create',
                        'method'     => [\System\Controller\SystemController::HTTP_GET, \System\Controller\SystemController::HTTP_POST],
                    ),
                ),
            ),
            'cityDelete' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/city/delete[/:id]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\City',
                        'action'     => 'delete',
                        'method'     => [\System\Controller\SystemController::HTTP_DELETE],
                        'id'         => null
                    ),
                ),
            ),
            'languageRecords' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/languages',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Language',
                        'action'     => 'getList',
                        'method'     => [\System\Controller\SystemController::HTTP_GET, \System\Controller\SystemController::HTTP_POST],
                        'limit'      => 20,
                        'offset'     => 0
                    ),
                ),
            ),
            'languageCreate' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/language/create',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Language',
                        'action'     => 'create',
                        'method'     => [\System\Controller\SystemController::HTTP_GET, \System\Controller\SystemController::HTTP_POST],
                    ),
                ),
            ),
            'languageUpdate' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/language/update',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Language',
                        'action'     => 'update',
                        'method'     => [\System\Controller\SystemController::HTTP_POST],
                    ),
                ),
            ),
            'languageUpdate2' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/languages/update',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Language',
                        'action'     => 'update',
                        'method'     => [\System\Controller\SystemController::HTTP_POST],
                    ),
                ),
            ),
            'languageShow' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/language/:id[/]',
                    'constraints' => [
                        'id' => '[A-Za-z]+'
                    ],
                    'defaults' => array(
                        'controller' => 'Application\Controller\Language',
                        'action'     => 'show',
                        'method'     => [\System\Controller\SystemController::HTTP_GET, \System\Controller\SystemController::HTTP_POST],
                    ),
                ),
            ),
            'countryRecords' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/countries',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Country',
                        'action'     => 'getList',
                        'method'     => [\System\Controller\SystemController::HTTP_GET, \System\Controller\SystemController::HTTP_POST],
                        'limit'      => 20,
                        'offset'     => 0
                    ),
                ),
            ),
            'countryUpdate' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/country/update',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Country',
                        'action'     => 'update',
                        'method'     => [\System\Controller\SystemController::HTTP_POST],
                    ),
                ),
            ),
            'countryUpdate2' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/countries/update',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Country',
                        'action'     => 'update',
                        'method'     => [\System\Controller\SystemController::HTTP_POST],
                    ),
                ),
            ),
            'countryShow' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/country/:id[/]',
                    'constraints' => [
                        'id' => '[A-Za-z]+'
                    ],
                    'defaults' => array(
                        'controller' => 'Application\Controller\Country',
                        'action'     => 'show',
                        'method'     => [\System\Controller\SystemController::HTTP_GET, \System\Controller\SystemController::HTTP_POST],
                    ),
                ),
            ),
            'countryCreate' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/country/create',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Country',
                        'action'     => 'create',
                        'method'     => [\System\Controller\SystemController::HTTP_GET, \System\Controller\SystemController::HTTP_POST],
                    ),
                ),
            ),
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'application' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/application',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'pl_PL',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'Application\Controller\City' => 'Application\Controller\CityController',
            'Application\Controller\Language' => 'Application\Controller\LanguageController',
            'Application\Controller\Country' => 'Application\Controller\CountryController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => [
            'ViewJsonStrategy'
        ]
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            'application_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/Application/Entity',  // Define path of entities
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Application\Entity' => 'application_driver'  // Define namespace of entities
                )
            )
        )
    ),
);
